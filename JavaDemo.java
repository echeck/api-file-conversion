import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Base64;


class JavaDemo {
    public static String toBase64String(String filePath) throws IOException {
        var fileBytes = Files.readAllBytes(Paths.get(filePath));
        var base64String = Base64.getEncoder().encodeToString(fileBytes);

        return URLEncoder.encode(base64String, StandardCharsets.UTF_8);
    }

    public static void fromBase64String(String base64String, String savePath) throws IOException {
        var decodeString = URLDecoder.decode(base64String, StandardCharsets.UTF_8);
        var bytes = Base64.getDecoder().decode(decodeString);

        Files.write(Paths.get(savePath), bytes, StandardOpenOption.CREATE);
    }
}