class CSharpDemo
{
    /// <summary>
    /// 将文件转为Base64字符串
    /// </summary>
    public static string ToBase64String(string filePath)
    {
        var fileBytes = System.IO.File.ReadAllBytes(filePath); //读取文件内容为字节数组
        var base64String = System.Convert.ToBase64String(fileBytes); //转为base64字符串

        return System.Web.HttpUtility.UrlEncode(base64String, System.Text.Encoding.UTF8); //对base64字符串做UrlEncode
    }

    /// <summary>
    /// 将Base64字符串还原为文件
    /// </summary>
    public static void FromBase64String(string base64String, string savePath)
    {
        var decodedString = System.Web.HttpUtility.UrlDecode(base64String, System.Text.Encoding.UTF8); //UrlDecode解码base64字符串
        var fileBytes = System.Convert.FromBase64String(decodedString); //还原为字节数组

        System.IO.File.WriteAllBytes(savePath, fileBytes); //将文件内容写入指定文件中
    }
}